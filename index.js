const express = require('express');
const mongoose = require('mongoose')
const cors = require('cors');
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')
const dotenv = require('dotenv')
// Port Variable
const port = process.env.PORT || 5000;
const app = express();

dotenv.config()

// MongoDB Connection
mongoose.connect(process.env.ATLAS_URI, {
    useNewUrlParser: true, 
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console,'Error connecting to Database'))
db.once('open', () => console.log('MongoDB connection is now established'))

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/users', userRoutes, orderRoutes)
app.use('/products', productRoutes)

app.listen(port, () => console.log(`Server is running on port ${port}`))