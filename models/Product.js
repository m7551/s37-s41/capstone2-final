const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please input a product name.']
    },
    description: {
        type: String,
        required: [true, 'Please input a product description.']
    },
    price: {
        type: Number,
        required: [true, 'Please input the price']
    },
    category: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Product', productSchema)