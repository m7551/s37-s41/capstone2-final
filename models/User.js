const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "Please input your first name"],
  },
  lastName: {
    type: String,
    required: [true, "Please input your last name"],
  },
  email: {
    type: String,
    required: [true, "Please input an email."],
  },
  mobileNo: {
    type: Number,
    required: [true, "Please input your mobile number"],
  },
  password: {
    type: String,
    min: 5,
    required: [true, `Please input an password.`],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  orders: [
    {
      orderId: { type: String, required: true },
      status: { type: String, default: "Processing" },
      totalAmount: { type: Number, required: true },
    },
  ],
  cart: [
    {
      productId: { type: String },
      name: { type: String },
      price: { type: Number },
      quantity: { type: Number, min: 1 },
      subtotal: { type: Number },
    },
  ],
  cartTotal: { type: Number },
});

module.exports = mongoose.model('User', userSchema)

