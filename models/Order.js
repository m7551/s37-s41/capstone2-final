const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User ID is required."],
  },
  email: { type: String },
  products: [
    {
      productId: String,
      name: String,
      price: { type: Number },
      quantity: {
        type: Number,
        min: [1, "Minimum of 1 order"]
      },
      subtotal: { type: Number }
    }
  ],
  totalAmount: {
    type: Number,
    required: [true, "Please input a total."],
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
  status: {
    type: String,
    default: "Processing",
  },
});

module.exports = mongoose.model("Order", orderSchema);
