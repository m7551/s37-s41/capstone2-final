const express = require("express");
const router = express.Router();
const orderController = require("../controller/orderController");
const auth = require("../auth");

// Order Creation - FINAL v2
router.post("/checkout", auth.verify, auth.notAdmin, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.addOrder(userData, req.body).then((result) => {
    console.log(`RESULT ${result}`)
    if (result === "NA") {
      res.status(400).send("Out of Stock");
    } else if (result === true) {
      res.send(result);
    } else {
      res.status(404).send("Product removed or does not exist");
    }
  });
});

// Retrieve authenticated user's order
router.get("/myOrders", auth.verify, auth.notAdmin, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController.getOrder(userData).then((result) => {
    res.send(result);
  });
});

// Retrieve all orders (ADMIN)
router.get("/orders", auth.verify, auth.isAdmin, (req, res) => {
  orderController.getAllOrder().then((result) => {
    res.send(result);
  });
});

// Order Receive (Non-Admin)
router.put("/:id/receive", auth.verify, auth.notAdmin, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController.receiveOrder(req.params, userData).then((result) => {
    console.log(`RESULT: ${result}`)
    if (result === "NG") {
      res.status(400).send("Error");
    } else if (result === "OR") {
      res.send("Order Already received");
    } else if (result === "404") {
      res.status(404).send("Order not found");
    } else if (result == true) {
      res.send(result);
    } else {
      res.status(500).send("Server Timeout");
    }
  });
});

module.exports = router;
