const express = require("express");
const router = express.Router();
const userController = require("../controller/userController");
const auth = require("../auth");
const { route } = require("express/lib/application");

//Get all user
router.get("/all", (req, res) => {
  userController.getAllUsers().then((result) => res.send(result));
});


// Get Specific user
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization)
  userController.getUser({userId: userData.id}).then(result => res.send(result))
})

// Create a user
router.post("/signup", (req, res) => {
  userController.signUpUser(req.body).then((result) => {
    res.send(result)
  });
});

// Email Checker
router.post("/checkEmail", (req, res) => {
  userController.checkEmail(req.body).then(result => {
    res.send(result)
  })
})

// User Authorization (Login)
router.post("/login", (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.loginUser(req.body).then((result) => {
    res.send(result);
  });
});

// Setting User as Admin
router.put("/:id/setAsAdmin", auth.verify, auth.isAdmin, (req, res) => {
  userController.adminUser(req.params.id).then((result) => {
    if (result === "NA") {
      res.status(404).send("Account not found");
    } else if (result) {
      res.send(result);
    } else {
      res.status(500).send("Error setting as admin");
    }
  });
});

module.exports = router;
