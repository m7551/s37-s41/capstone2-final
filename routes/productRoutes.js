const express = require("express");
const router = express.Router();
const productController = require("../controller/productController");
const auth = require("../auth");

// Retrieve all product
router.get("/all", (req, res) => {
  productController.getAllProducts().then((result) => res.send(result));
});

// Retrieve All ACTIVE product
router.get("/", (req, res) => {
  productController.getActiveProducts().then((result) => res.send(result));
});

// Creating a product (Admin Only)
router.post("/", auth.verify, auth.isAdmin, (req, res) => {
  productController.addProduct(req.body).then((result) => {
    console.log(result)
    res.send(result);
  });
});

// Search a product
router.post("/search", (req, res) => {
  productController.searchProduct(req.body).then((result) => res.send(result));
});

// Retrieve a single product
router.get("/:id", (req, res) => {
  productController.getProduct(req.params.id).then((result) => {
    if (result === false) {
      res.status(404).send(result);
    } else if (result) {
      res.send(result);
    } else {
      res.status(500).send("Server Timeout");
    }
  });
});

// Updating Product Info (Admin Only)
router.put("/:id", auth.verify, auth.isAdmin, (req, res) => {
  const updatedData = {
    name: req.body.name,
    description: req.body.description,
    brand: req.body.brand,
    price: req.body.price,
    category: req.body.category,
  };
  productController.updateProduct(req.params, updatedData).then((result) => {
    if (result === "NA") {
      res.status(404).send("Product does not exist");
    } else if (result) {
      res.send(result);
    } else {
      res.status(500).send("Server Timeout");
    }
  });
});

// Archiving a Product (Admin Only)
router.get("/:id/archive", auth.verify, auth.isAdmin, (req, res) => {
  productController.archiveProduct(req.params.id).then((result) => {
    if (result === "NA") {
      res.status(404).send("Product does not exist");
    } else if (result) {
      res.send(result);
    } else {
      res.status(500).send("Server Timeout");
    }
  });
});

// Return product to Active (Admin Only)
router.get("/:id/unarchive", auth.verify, auth.isAdmin, (req, res) => {
  productController.unarchiveProduct(req.params.id).then((result) => {
    if (result === "NA") {
      res.status(404).send("Product does not exist");
    } else if (result) {
      res.send(result);
    } else {
      res.status(500).send("Server Timeout");
    }
  });
});

// Delete product
router.delete("/:id/deleteProduct", auth.verify, auth.isAdmin, (req, res) => {
  productController.deleteProduct(req.params.id).then((result) => {
        if (result === "NA") {
      res.status(404).send("Product does not exist");
    } else if (result) {
      res.send(result);
    } else {
      res.status(500).send("Server Timeout");
    }
  })
})

module.exports = router;
