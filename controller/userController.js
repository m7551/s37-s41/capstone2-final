const User = require("../models/User");
const Product = require('../models/Product');
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Get all user
module.exports.getAllUsers = () => {
  return User.find({}).then((result) => {
    return result;
  });
};


// Get Specific User
module.exports.getUser = (data) => {
  return User.findById(data.userId).then(result => {
    result.password = "";
    return result;
  })
}



// Sign Up User
module.exports.signUpUser = (reqBody) => {
  let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
  })

  return newUser.save().then((createdUser, error) => {
    if(error){
      return false
    } else {
      return true
    }
  })

};

// Email checker
module.exports.checkEmail = (reqBody) => {
  return User.find({email: reqBody.email}).then(result => {
    if(result.length > 0){
      return true
    } else {
      return false
    }
  })
}

// Login User
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      // If email is not registered
      return false;
    } else {
      const pwCorrect = bcrypt.compareSync(reqBody.password, result.password);

      if (pwCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        // If password is incorrect
        return false;
      }
    }
  });
};

// Setting user as admin
module.exports.adminUser = (reqParams) => {
  return User.findById(reqParams)
    .then((result) => {
      if (result.isAdmin) {
        return "Already Admin";
      } else {
        result.isAdmin = true;

        return result.save().then((result, err) => {
          if (err) {
            return false;
          } else {
            return "You now have admin privilege.";
          }
        });
      }
    })
    .catch((error) => {
      return "NA";
    });
};
