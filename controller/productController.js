const Product = require("../models/Product");

// Retrieve All Product
module.exports.getAllProducts = () => {
  return Product.find({}).then((result) => {
    return result;
  });
};

// Retrieve Single Product
module.exports.getProduct = (reqParams) => {
  return Product.findById(reqParams)
    .then((result) => {
      if (result) {
        return result;
      }
    })
    .catch((error) => {
      return false;
    });
};

// Retrieve All ACTIVE product
module.exports.getActiveProducts = () => {
  return Product.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Creating a new product
module.exports.addProduct = (reqBody) => {
  return Product.findOne({ name: reqBody.name }).then((result) => {
    // Check if the product is already exists
    if (result) {
      return false
    } else {
      let newProduct = new Product({
        name: reqBody.name,
        brand: reqBody.brand,
        description: reqBody.description,
        price: reqBody.price,
        category: reqBody.category,
      });

      return newProduct.save().then((newProduct, err) => {
        if (err) {
          return false
        } else {
          return true;
        }
      });
    }
  });
};

// Updating a product (Admin)
module.exports.updateProduct = (reqParams, updatedData) => {
  return Product.findById(reqParams.id)
    .then((result) => {
      if (result) {
        // If admin chose to update just one property, there will be no error
        (result.name = updatedData.name ? updatedData.name : result.name),
          (result.brand = updatedData.brand ? updatedData.brand : result.brand),
          (result.description = updatedData.description
            ? updatedData.description
            : result.description),
          (result.price = updatedData.price ? updatedData.price : result.price);
        result.category = updatedData.category
          ? updatedData.category
          : result.category;

        return result.save().then((updatedProduct, err) => {
          if (err) {
            return `Error saving product`;
          } else {
            return true;
          }
        });
      } else {
        return error;
      }
    })
    .catch((error) => {
      return "NA";
    });
};

// Archiving a Product (Admin Only)
module.exports.archiveProduct = (reqParams) => {
  return Product.findById(reqParams)
    .then((result) => {
      // Check if the product is already archived
      if (result.isActive) {
        result.isActive = false;

        return result.save().then((archivedProduct, err) => {
          if (err) {
            return "Error archiving product";
          } else {
            return "Product Archived";
          }
        });
      } else {
        return "Product already archived";
      }
    })
    .catch((error) => {
      return "NA";
    });
};

// Unarchiving a Product (Admin Only)
module.exports.unarchiveProduct = (reqParams) => {
  return Product.findById(reqParams)
    .then((result) => {
      // Check if the product is already archived
      if (result.isActive === false) {
        result.isActive = true;

        return result.save().then((archivedProduct, err) => {
          if (err) {
            return "Error unarchiving product";
          } else {
            return "Product is now active";
          }
        });
      } else {
        return "Product is already active";
      }
    })
    .catch((error) => {
      return "NA";
    });
};

// Search Product
module.exports.searchProduct = (reqBody) => {
  let query = new RegExp(reqBody.search, "i");
  if (reqBody.search != null && reqBody.search != "") {
    return Product.find({
      category: query,
    }).then((result) => {
      if (result.length <= 0) {
        return "No results";
      } else {
        return result;
      }
    });
  } else {
    return false;
  }
};

// Delete Product
module.exports.deleteProduct = (reqParams) => {
  return Product.findByIdAndDelete(reqParams).then((result) => {
    if(result){
      return "Product deleted"
    }
  })
  .catch((error) => {
    return "NA";
  })
}
