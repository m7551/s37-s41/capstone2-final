const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

// Get User's order
module.exports.getOrder = (data) => {
  // Find the Order through userId but will exclude the userId in result
  return Order.find({ userId: data.id }, { userId: 0, email: 0 })
    .then((result) => {
      return result;
    })
    .catch((error) => {
      return "Error order not found";
    });
};

// Get all order (Admin only)
module.exports.getAllOrder = () => {
  return Order.find({})
    .then((result) => {
      return result;
    })
    .catch((error) => {
      return "Error retrieving orders";
    });
};

// Update Order
module.exports.receiveOrder = (reqParams, userData) => {
  return Order.findById(reqParams.id)
    .then((orderResult) => {
      if (userData.id == orderResult.userId) {
        if (orderResult.status == "Received") {
          return "OR";
        } else {
          orderResult.status = "Received";
          return orderResult.save().then((result, error) => {
            if (error) {
              return false;
            } else {
              return true;
            }
          });
        }
      } else {
        return "404";
      }
    })
    .catch((error) => {
      return "404"
    });
};


// Order Creation FINAL
module.exports.addOrder = async (userData, reqBody) => {
  let order = reqBody.products;
  let total = 0;
  let checker = [];
  let productStatuses = [];

  // Check if the product exist
  for (let i = 0; i < order.length; i++) {
    await Product.findById(order[i].productId)
      .then((result) => {
        if (result == null) {
          return false;
        } else {
          checker.push(1);
        }
      })
      .catch((error) => {
        return error;
      });
  }

  // Checking if there's inactive Products
  for (let c = 0; c < order.length; c++) {
    await Product.findById(order[c].productId)
      .then((result) => {
        productStatuses.push(result.isActive);
      })
      .catch((error) => {
        return error;
      });
  }

  let productStatus = productStatuses.some((z) => z === false);
  // Check if the order in req.body and checker matches
  if (checker.length == order.length) {
    if (productStatus === false) {
      for (let a = 0; a < order.length; a++) {
        let price = await Product.findById(order[a].productId).then(
          (result) => {
            return result.price;
          }
        );
        let name = await Product.findById(order[a].productId).then((result) => {
          return result.name;
        });

        order[a].name = name;
        order[a].price = price;
        order[a].subtotal = order[a].quantity * order[a].price;
      }

      for (let b = 0; b < order.length; b++) {
        total += order[b].subtotal;
      }

      let newOrder = new Order({
        userId: userData.id,
        email: userData.email,
        products: order,
        totalAmount: total,
      });

      let orderSaved = await newOrder.save().then((order, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });

      let isUserUpdated = await User.findById(userData.id).then((user) => {
        user.orders.push({
          orderId: newOrder._id,
          totalAmount: newOrder.totalAmount,
          status: newOrder.status,
        });

        return user.save().then((savedUser, error) => {
          if (error) {
            return false;
          } else {
            return true;
          }
        });
      });

      if (isUserUpdated && orderSaved) {
        return true;
      } else {
        return "Error creating order";
      }
    } else { //If the productStatus is false (if the product is out of stock)
      return "NA";
    }
  } else { //If the checker and req.body did not match
    return "Error product Id";
  }
};