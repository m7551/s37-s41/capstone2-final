const dotenv = require('dotenv')
const jwt = require('jsonwebtoken');

dotenv.config()

// Token Creation
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        name: user.firstName,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, process.env.SECRET, {})
}

// Token Verification
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;
    if(typeof token !=='undefined'){
        token = token.slice(7, token.length)

        return jwt.verify(token, process.env.SECRET, (err, data) => {
            if (err){
                return res.status(401).send('Authentication Failed')
            } else {
                next()
            }
        })
    } else {
        return res.status(401).send('Authentication Failed')
    }
}

// Token Decoding
module.exports.decode = (token) => {
    if(typeof token !== 'undefined'){
        token = token.slice(7, token.length)
        
        return jwt.verify(token, process.env.SECRET, (err, data) => {
            if(err){
                return null
            } else {
                return jwt.decode(token, {complete: true}).payload
            }
        })
    } else{
        return null
    }
}


// Privilege checking (isAdmin)
module.exports.isAdmin = (req, res, next) => {
    let token = req.headers.authorization

    token = token.slice(7, token.length)

    let data = jwt.decode(token)

    if(data.isAdmin){
        next()
    } else {
        return res.status(403).send('Not authorized to take the action')
    }
}

// Privilege checking (notAdmin)
module.exports.notAdmin = (req, res, next) => {
    let token = req.headers.authorization

    token = token.slice(7, token.length)

    let data = jwt.decode(token)

    if(data.isAdmin === false){
        next()
    } else {
        return res.status(403).send('Only customers can perform this action')
    }
}